# Package fastqc version 0.11.8
https://www.bioinformatics.babraham.ac.uk/projects/fastqc/


FastQC aims to provide a simple way to do some quality control checks on raw sequence data coming from high throughput sequencing pipelines. It provides a modular set of analyses which you can use to give a quick impression of whether your data has any problems of which you should be aware before doing any further analysis.

Package installation using Miniconda3 V4.7.10 All packages are in /opt/miniconda/bin & are in PATH fastqc Version: 0.11.8
Singularity container based on the recipe: Singularity.fastqc_v0.11.8
## Local build:
sudo singularity build ackage installation using Miniconda3 V4.7.10 All packages are in /opt/miniconda/bin & are in PATH fastqc Version: 0.11.8
Singularity container based on the recipe: Singularity.fastqc_v0.11.8

```bash
sudo singularity build fastqc_v0.11.8.sif Singularity.fastqc_v0.11.8
```
## Get image help:

```bash
singularity run-help fastqc_v0.11.8.sif
```
### Default runscript: fastqc
## Usage:
```bash
./fastqc_v0.11.8.sif --help
```
or:
```bash
singularity exec fastqc_v0.11.8.sif samtools --help
```
image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:


```bash
singularity pull fastqc_v0.11.8.sif oras://registry.forgemia.inra.fr/singularity/bioinfo/fastqc_v0.11.8/fastqc_v0.11.8:latest
```
